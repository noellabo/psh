# PSH
Edge ActivityPub

## ZEIT Now
`now`

## Bookmarks
[bookmarks.md](https://gitlab.com/tkithrta/psh/blob/master/docs/bookmarks.md)

## Keywords
Flask==1.0.2 Jinja2==2.10 MarkupSafe==1.0 Werkzeug==0.14.1 click==6.7 itsdangerous==0.24  
httpsig==1.2.0 pycryptodome==3.4.7 six==1.11.0  
certifi==2018.4.16 chardet==3.0.4 idna==2.6 requests==2.19.0 urllib3==1.22

pip Flask==1.0.2 httpsig==1.2.0 requests==2.19.0  
dir float id index name path  
python app body  
psh private public key  
req res headers  
x y inbox accept actor activity content sign auth host method value  
i j k admin guest user err status mailto acct xmpp init main  
api pub webfinger outbox z u s users statuses url ip form text submit route

[keywords.json](https://gitlab.com/tkithrta/psh/blob/master/docs/keywords.json)