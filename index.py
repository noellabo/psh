# -*- coding: utf-8 -*-
"""PSH 0.0.2

"""

from datetime import datetime
import json
from urllib.parse import urlparse
from flask import Flask, Response, jsonify, redirect, request, url_for
from httpsig import HeaderSigner
import requests
from werkzeug.http import http_date
from Crypto import Random
from Crypto.PublicKey import RSA

private_key, public_key = "", ""
app = Flask(__name__)


def actor_inbox(actor):
    req = requests.get(actor + ".json")
    print("actor get") if req.ok else print("actor get error %r %r" % (req.status_code, req.headers))
    return req.json()["inbox"]


def generate_key_pair():
    rsa = RSA.generate(1024, Random.new().read)
    return [rsa.exportKey().decode("utf-8"), rsa.publickey().exportKey().decode("utf-8")]


def sign_headers(actor_host, method, path):
    host = request.host
    name = "a"
    key_id = "https://" + host + "/u/" + name + "/publickey"
    sign = HeaderSigner(key_id, private_key, algorithm="rsa-sha256", headers=["(request-target)", "host", "date"]).sign(
        { "Host": actor_host, "Date": http_date() }, method=method, path=path
    )
    auth = sign.pop("authorization")
    sign["Signature"] = auth[len("Signature ") :] if auth.startswith("Signature ") else ""
    sign["User-Agent"] = "Python/3.6 (PSH/0.0.2; +https://" + host + "/)"
    sign["Accept-Encoding"] = "gzip"
    sign["Accept"] = "application/json, application/jrd+json, application/ld+json, application/activity+json"
    return sign


def accept(inbox, activity):
    host = request.host
    name = "a"
    float_id = datetime.now().timestamp()
    res = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://" + host + "/u/" + name + "/s/" + str(float_id),
        "type": "Accept",
        "actor": "https://" + host + "/u/" + name,
        "object": activity,
    }
    headers = sign_headers(urlparse(inbox).netloc, "POST", urlparse(inbox).path)
    req = requests.post(inbox, json=res, headers=headers)
    print("accept post") if req.ok else print("accept post error %r %r" % (req.status_code, req.headers))


def create_note(inbox, content):
    host = request.host
    name = "a"
    float_id = datetime.now().timestamp()
    res = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://" + host + "/u/" + name + "/s/" + str(float_id),
        "type": "Create",
        "object": {
            "id": "https://" + host + "/u/" + name + "/s/" + str(float_id),
            "type": "Note",
            "published": datetime.fromtimestamp(float_id).isoformat(),
            "url": "https://" + host + "/u/" + name + "/s/" + str(float_id),
            "attributedTo": "https://" + host + "/u/" + name,
            "to": "https://www.w3.org/ns/activitystreams#Public",
            "content": "<p>" + content + "</p>",
        },
    }
    headers = sign_headers(urlparse(inbox).netloc, "POST", urlparse(inbox).path)
    req = requests.post(inbox, json=res, headers=headers)
    print("create note post") if req.ok else print("create note post error %r %r" % (req.status_code, req.headers))


def follow(inbox):
    host = request.host
    name = "a"
    float_id = datetime.now().timestamp()
    res = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://" + host + "/u/" + name + "/s/" + str(float_id),
        "type": "Follow",
        "actor": "https://" + host + "/u/" + name,
        "object": "https://example.com/users/z",
    }
    headers = sign_headers(urlparse(inbox).netloc, "POST", urlparse(inbox).path)
    req = requests.post(inbox, json=res, headers=headers)
    print("follow post") if req.ok else print("follow post error %r %r" % (req.status_code, req.headers))


@app.before_request
def before():
    print("%s %r" % (request.url, request.headers))


@app.route("/")
def index():
    return Response("PSH")


@app.route("/u")
def u():
    return Response("PSH index.py")


@app.route("/s")
def s():
    res = str(datetime.now().timestamp())
    return Response(res)


@app.route("/u/a/s")
def us():
    return Response(status=404)


@app.route("/u/a/s/<float:float_id>")
def us_id(float_id):
    return redirect(url_for("u"))


@app.route("/u/a")
def uu():
    host = request.host
    name = "a"
    res = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://" + host + "/u/" + name,
        "type": "Person",
        "inbox": "https://" + host + "/u/" + name + "/inbox",
        "outbox": "https://" + host + "/u/" + name + "/outbox",
        "preferredUsername": name,
        "name": "a",
        "summary": "0.0.2",
        "url": "https://" + host + "/u/" + name,
        "icon": {
            "type": "Image",
            "mediaType": "image/png",
            "url": "https://" + host + "/static/" + name + "u.png",
        },
        "image": {
            "type": "Image",
            "mediaType": "image/png",
            "url": "https://" + host + "/static/" + name + "s.png",
        },
    }
    return Response(json.dumps(res), headers={ "Content-Type": "application/activity+json" })


@app.route("/u/a/publickey")
def pub():
    host = request.host
    name = "a"
    res = {
        "@context": [
            "https://www.w3.org/ns/activitystreams",
            "https://w3id.org/security/v1",
        ],
        "publicKey": {
            "id": "https://" + host + "/u/a/publickey",
            "type": "Key",
            "owner": "https://" + host + "/u/a",
            "publicKeyPem": public_key,
        },
    }
    return Response(json.dumps(res), headers={ "Content-Type": "application/activity+json" })


@app.route("/u/a/inbox", methods=["POST"])
def inbox():
    global private_key, public_key
    if request.headers["Content-Type"] != "application/activity+json" or "type" not in request.json:
        return Response(status=400)
    x = request.json
    if type(x) != dict or "type" not in x:
        return Response(status=400)
    elif x["type"] == "Create" and "actor" in x:
        try:
            private_key, public_key = generate_key_pair()
            inbox = actor_inbox(x["actor"])
            create_note(inbox, "Poi")
        except:
            return Response(status=500)
        return Response(status=200)
    elif x["type"] == "Follow" and "actor" in x:
        try:
            private_key, public_key = generate_key_pair()
            inbox = actor_inbox(x["actor"])
            accept(inbox, x)
        except:
            return Response(status=500)
        return Response(status=200)
    elif x["type"] == "Undo":
        y = x["object"]
        if type(y) != dict or "type" not in y:
            return Response(status=400)
        elif y["type"] == "Follow" and "actor" in y:
            try:
                private_key, public_key = generate_key_pair()
                inbox = actor_inbox(y["actor"])
                accept(inbox, x)
            except:
                return Response(status=500)
            return Response(status=200)
    return Response(status=501)


@app.route("/u/a/outbox", methods=["POST"])
def outbox():
    return Response(status=501)


@app.route("/s/xa00000")
def xa00000():
    res = { "key": "value" }
    res["a"] = request.host
    return jsonify(res)


@app.route("/.well-known/webfinger")
def webfinger():
    host = request.host
    name = "a"
    res = {
        "subject": "acct:" + name + "@" + host,
        "links": [
            {
                "rel": "self",
                "type": "application/activity+json",
                "href": "https://" + host + "/u/" + name,
            }
        ],
    }
    return jsonify(res)


if __name__ == "__main__":
    app.run()